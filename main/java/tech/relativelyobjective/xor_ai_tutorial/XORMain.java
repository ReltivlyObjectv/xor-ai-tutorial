package tech.relativelyobjective.xor_ai_tutorial;

/**
 *
 * @author reltivlyobjectv
 */
public class XORMain {

    public static final int NEURONS_PER_COLUMN = 3;
    public static final int INPUT_VALUES_COUNT = 2;
    private static final int TIMES_TO_TRAIN = 3;
    
    public static void main(String[] args) {
        //Setup Data Model
        DataModel model = new DataModel(NEURONS_PER_COLUMN, INPUT_VALUES_COUNT);
        //Perform Calculation
        trainAndLogToConsole(1f,1f,0f,model);
        trainAndLogToConsole(0f,0f,0f,model);
        trainAndLogToConsole(1f,0f,1f,model);
        trainAndLogToConsole(0f,1f,1f,model);
        
        for (int i = 0; i < 2; i++) {
            for (int j = 0; j < 2; j++) {
                double[] inputs = new double[2];
                inputs[0] = (double) i;
                inputs [1] = (double) j;
                System.out.printf("Inputs: (%d,%d)\tOutput: %f\n", i, j, model.calculateOutput(inputs));
            }
        }
        
    }
    private static void trainAndLogToConsole(double inputA, double inputB, double expectedoutput, DataModel model) {
        double[] inputs = new double[INPUT_VALUES_COUNT];
        //Train Both Positive
        inputs[0] = inputA;
        inputs[1] = inputB;
        double expectedOutput = expectedoutput;
        System.out.println("Expected Output: "+expectedOutput);
        System.out.println("Initial Output: "+model.calculateOutput(inputs));
        for (int i = 0; i < TIMES_TO_TRAIN; i++) {
            model.train(inputs, expectedOutput);
        }
        System.out.printf("Output after training %d times: %f\n", TIMES_TO_TRAIN, model.calculateOutput(inputs));
    }
}
