package tech.relativelyobjective.xor_ai_tutorial;

/**
 *
 * @author reltivlyobjectv
 */
public class InputNeuron extends Neuron {
    protected double value;
    
    @Override
    public double getCalculatedValue() {
        return value;
    }
    public void setValue(double newValue) {
        value = newValue;
    }
    public double getValue() {
        return value;
    }
}
