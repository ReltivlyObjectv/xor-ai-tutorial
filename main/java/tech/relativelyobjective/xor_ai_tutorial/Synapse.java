package tech.relativelyobjective.xor_ai_tutorial;

import java.util.Random;

/**
 *
 * @author reltivlyobjectv
 */
public class Synapse {
    private Neuron dest, source;
    private double weight;
    public Synapse(Neuron destinationNeuron, Neuron sourceNeuron) {
        dest = destinationNeuron;
        source = sourceNeuron;
        weight = new Random().nextDouble();
    }
    public Synapse(Neuron destinationNeuron, Neuron sourceNeuron, double weight) {
        dest = destinationNeuron;
        source = sourceNeuron;
        this.weight = weight;
    }
    public Neuron getDestination() {
        return dest;
    }
    public Neuron getSource() {
        return source;
    }
    public double getWeight() {
        return weight;
    }
    public void setWeight(double newWeight) {
        weight = newWeight;
    }
}
