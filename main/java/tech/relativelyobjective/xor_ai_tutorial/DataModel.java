package tech.relativelyobjective.xor_ai_tutorial;

import java.util.AbstractMap.SimpleEntry;
import java.util.LinkedList;
import java.util.Map;

/**
 *
 * @author reltivlyobjectv
 */
public class DataModel {
    private final InputNeuron[] inputs;
    private final Neuron[] hiddenNeurons;
    Neuron finalValue;
    
    public DataModel(int neuronsPerColumn, int inputValuesCount) {
        //System.out.println("Creating Initial Nodes: (1,1)");
        //Create Neurons
        inputs = new InputNeuron[inputValuesCount];
        hiddenNeurons = new Neuron[neuronsPerColumn];
        finalValue = new Neuron(null, new LinkedList<>());
        System.out.println("Created Final Value Neuron: "+finalValue.getUUID());
        System.out.println();
        for (int inputNode = 0; inputNode < inputs.length; inputNode++) {
            inputs[inputNode] = new InputNeuron();
            System.out.println("Created Input Value Neuron #"+inputNode+": "+inputs[inputNode].getUUID().toString());
            
        }
        System.out.println();
        for (int hiddenNodes = 0; hiddenNodes < neuronsPerColumn; hiddenNodes++) {
            hiddenNeurons[hiddenNodes] = new Neuron();
            System.out.println("Created Hidden Neuron #"+hiddenNodes+": "+hiddenNeurons[hiddenNodes].getUUID().toString());
        }
        //Add Synapses From Inputs to Hidden
        System.out.println();
        System.out.println("Creating Synapses from Inputs to Hidden Nodes");
        for (int i = 0; i < inputs.length; i++) {
            for (int j = 0; j < hiddenNeurons.length; j++) {
                Synapse newSynapse = new Synapse(hiddenNeurons[j], inputs[i]);
                System.out.printf("Created Synapse: %s -> %s\n", newSynapse.getSource().getUUID(), newSynapse.getDestination().getUUID());
                inputs[i].getOutgoingSynapses().add(newSynapse);
                hiddenNeurons[j].getIncomingSynapses().add(newSynapse);
            }
        }
        //Add Synapses From Hidden to Output
        System.out.println();
        System.out.println("Creating Synapses from Hidden Nodes to Output Node");
        for (int i = 0; i < hiddenNeurons.length; i++) {
            Synapse newSynapse = new Synapse(finalValue, hiddenNeurons[i]);
            System.out.printf("Created Synapse: %s -> %s\n", newSynapse.getSource().getUUID(), newSynapse.getDestination().getUUID());
            hiddenNeurons[i].getOutgoingSynapses().add(newSynapse);
            finalValue.getIncomingSynapses().add(newSynapse);
        }
    }
    public double calculateOutput(double[] inputs) {
        if (inputs.length != this.inputs.length) {
            throw new ArrayIndexOutOfBoundsException(
                "The array passed in to calculateOutput must have the same length as the expected inputs count: "+inputs.length
            );
        }
        for (int i = 0; i < inputs.length; i++) {
            this.inputs[i].setValue(inputs[i]);
        }
        return finalValue.getCalculatedValue();
    }
    public void train(double[] inputs, double expectedOutput) {
        if (inputs.length != this.inputs.length) {
            throw new ArrayIndexOutOfBoundsException(
                "The array passed in to train method must have the same length as the expected inputs count: "+inputs.length
            );
        }
        //Note the original weights of hidden-to-outer neurons for later calculation
        LinkedList<Map.Entry<Neuron, Double>> originalHiddenToOuterWeights = new LinkedList<>();
        for (int i = 0; i < hiddenNeurons.length; i++) {
            originalHiddenToOuterWeights.add(new SimpleEntry<>(hiddenNeurons[i], hiddenNeurons[i].getOutgoingSynapses().getFirst().getWeight()));
        }
        //Adjust Weights between hidden layer and output layer
        double calculatedOutput = calculateOutput(inputs);
        double marginOfError = expectedOutput - calculatedOutput;
        double derivativeSum = Neuron.activationFunctionDerivative(marginOfError);
        double deltaSum = derivativeSum * marginOfError;
        System.out.printf("Output: %f\tExpected: %f\tDelta Sum: %f\n", calculatedOutput, expectedOutput, deltaSum);
        for (int i = 0; i < hiddenNeurons.length; i++) {
            double deltaWeight = deltaSum / hiddenNeurons[i].getCalculatedValue();
            //hiddenNeurons[i].setValue(hiddenNeurons[i].getValue() + deltaWeight);
            //Change weights to match delta weight
            Synapse synapse = hiddenNeurons[i].getOutgoingSynapses().getFirst();
            synapse.setWeight(synapse.getWeight() + deltaWeight);
        }
        //Adjust weights between input layer and hidden layer
        for (int i = 0; i < hiddenNeurons.length; i++) {
            Double originalOutputWeight = null;
            for (Map.Entry<Neuron, Double> value : originalHiddenToOuterWeights) {
                if (value.getKey() == hiddenNeurons[i]) {
                    originalOutputWeight = value.getValue();
                    break;
                }
            }
            if (originalOutputWeight == null) {
                System.out.println("The original output value for node is missing: "+hiddenNeurons[i].getUUID());
                continue;
            }
            double deltaHiddenSum = deltaSum / originalOutputWeight * Neuron.activationFunctionDerivative(hiddenNeurons[i].getCalculatedValue());
            for (Synapse synapse : hiddenNeurons[i].getIncomingSynapses()) {
                double deltaWeights = deltaHiddenSum / synapse.getSource().getCalculatedValue();
                synapse.setWeight(synapse.getWeight() + deltaWeights);
            }
        }
    }
}
