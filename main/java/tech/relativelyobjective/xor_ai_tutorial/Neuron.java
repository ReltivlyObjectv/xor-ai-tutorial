package tech.relativelyobjective.xor_ai_tutorial;

import java.util.LinkedList;
import java.util.Random;
import java.util.UUID;

/**
 *
 * @author reltivlyobjectv
 */
public class Neuron {
    private LinkedList<Synapse> next, previous;
    private UUID id;
    
    public Neuron() {
        next = new LinkedList<>();
        previous = new LinkedList<>();
        id = UUID.randomUUID();
    }
    public Neuron(LinkedList<Synapse> nextNodes) {
        next = nextNodes;
        previous = new LinkedList<>();;
        id = UUID.randomUUID();
    }
    public Neuron(LinkedList<Synapse> nextNodes, LinkedList<Synapse> previousNodes) {
        next = nextNodes;
        previous = previousNodes;
        id = UUID.randomUUID();
    }
    public Neuron(LinkedList<Synapse> nextNodes, LinkedList<Synapse> previousNodes, UUID uuid) {
        next = nextNodes;
        previous = previousNodes;
        id = uuid;
    }
    public LinkedList<Synapse> getIncomingSynapses() {
        return previous;
    }
    public LinkedList<Synapse> getOutgoingSynapses() {
        return next;
    }
    public double getCalculatedValue() {
        double sumProduct = 0f;
        for (Synapse incomingSynapse : previous) {
            double weightedInfluence = incomingSynapse.getWeight() * incomingSynapse.getSource().getCalculatedValue();
            if (Double.isNaN(weightedInfluence)) {
                System.out.println("Nan Encountered (Weighted Influence):\tSynapse Weight:"+incomingSynapse.getWeight()+"\tSource Value:"+incomingSynapse.getSource().getCalculatedValue());
            }
            sumProduct += weightedInfluence;
        }
        if (Double.isNaN(sumProduct)) {
            System.out.println("NaN Encountered (Calculated Val SumProduct)");
        }
        return activationFunction(sumProduct);
    }
    public static double activationFunction(double x) {
        //Sigmoid (logistic)
        double fOfX = 1f / (1 + Math.pow(Math.E, -x));
        if (Double.isNaN(fOfX)) {
            System.out.printf("NaN Encountered (Activation):\tInput:%f\tFormula: 1f / (1 + Math.pow(%f, %f))\n", x, Math.E, -x);
        }
        return fOfX;
    }
    public static double activationFunctionDerivative(double x) {
        //https://math.stackexchange.com/questions/78575/derivative-of-sigmoid-function-sigma-x-frac11e-x
        double fOfXDerivative = activationFunction(x) * (1f - activationFunction(x));
        if (Double.isNaN(fOfXDerivative)) {
            System.out.printf("NaN Encountered (Derivative):\tInput:%f\tFormula: %f * (1f - %f)\n", x, activationFunction(x), activationFunction(x));
        }
        return fOfXDerivative;
    }
    public UUID getUUID() {
        return id;
    }
}
